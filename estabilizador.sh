#!/bin/bash

mkdir procesados

for file in *.avi; do
    echo "+--------------------------------------------------------------------+"
    echo "    procesando archivo: $file"
    echo "+--------------------------------------------------------------------+"

    #transcode -J stabilize --mplayer_probe -i $file
    transcode -J stabilize=shakiness=6:accuracy=6    --mplayer_probe -i $file
    transcode -J transform=crop=0:optzoom=1 --mplayer_probe -i $file -y xvid4 -o procesados/pr-$file

    rm $file.trf

    echo "Video $file comnprimido y estabilizado!"
done

